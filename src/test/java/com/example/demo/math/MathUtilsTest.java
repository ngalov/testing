package com.example.demo.math;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertEquals;

class MathUtilsTest {

    private final MathUtils mathUtils = new MathUtilsImpl();

    @Test
    void scale() {
        assertEquals(BigDecimal.valueOf(0.1234),
                mathUtils.scale(BigDecimal.ONE,
                        0.1234), "must return double");
    }

}