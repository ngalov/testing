package com.example.demo.controller;

import com.example.demo.service.ConvertService;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@AutoConfigureWireMock(port = 4567)
class ConvertControllerComponentTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ConvertService convertService;

    @Test
    public void shouldReturnConvertRate() throws Exception {

        String from = "USD";
        String to = "EUR";
        String amount = "555";

        BigDecimal mockConvertResult = BigDecimal.ONE;

        Mockito.when(convertService.covert(from, to, new BigDecimal(amount))).thenReturn(mockConvertResult);
        mockMvc.perform(get("/converter")
                .queryParam("from", from)
                .queryParam("to", to)
                .queryParam("amount", amount))
                .andExpect(jsonPath("$.sourceAmount").value(amount))
                .andExpect(jsonPath("$.targetAmount").value(mockConvertResult))
                .andExpect(status().is2xxSuccessful());
    }
}