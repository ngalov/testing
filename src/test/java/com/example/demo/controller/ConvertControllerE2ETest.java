package com.example.demo.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.tomakehurst.wiremock.client.WireMock;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock;
import org.springframework.test.web.servlet.MockMvc;

import java.util.HashMap;
import java.util.Map;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.requestParameters;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@AutoConfigureWireMock(port = 4567)
@AutoConfigureRestDocs(outputDir = "target/snippets")
class ConvertControllerE2ETest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void shouldReturnConvertRate() throws Exception {

        String from = "USD";
        String to = "EUR";
        String amount = "555";

        Map<String, String> payload = new HashMap<>();
        payload.put("base", from);
        String json = new ObjectMapper().writeValueAsString(payload);

        stubFor(WireMock.get(urlEqualTo("/fetch-one?from=USD&to=EUR&api_key=3721e2ab07-a370c625a0-qr3qyt"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withBody("{\n" +
                                "  \"base\": \"USD\",\n" +
                                "  \"result\": {\n" +
                                "    \"EUR\": 0.84653\n" +
                                "  },\n" +
                                "  \"updated\": \"2021-04-05 17:43:35\",\n" +
                                "  \"ms\": 19\n" +
                                "}")));

        mockMvc.perform(get("/converter")
                .queryParam("from", from)
                .queryParam("to", to)
                .queryParam("amount", amount))
                .andExpect(jsonPath("$.sourceAmount").value(amount))
                .andExpect(jsonPath("$.targetAmount").value("469.82415"))
                .andExpect(status().is2xxSuccessful())
                .andDo(
                        document("converter",
                                requestParameters(
                                        parameterWithName("to").description("target currency"),
                                        parameterWithName("from").description("base currency"),
                                        parameterWithName("amount").description("amount to convert"))
                        ));
    }
}