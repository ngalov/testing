package com.example.demo.service;

import com.example.demo.client.ForexClient;
import com.example.demo.dto.ForexResult;
import com.example.demo.math.MathUtils;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;

import java.math.BigDecimal;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.eq;

@SpringBootTest
class ConvertServiceImplComponentTest {

    @Autowired
    private ConvertService convertService;

    @SpyBean
    MathUtils mathUtils;

    @MockBean
    ForexClient forexClient;

    @Captor
    private ArgumentCaptor<Double> argumentCaptor;

    @Test
    void covert() {

        String from = "from";
        String to = "to";
        BigDecimal amount = new BigDecimal("214");
        double expectedRate = 0.1;

        Mockito.when(forexClient.getForexData(from, to))
                .thenReturn(new ForexResult().setResult(Collections.singletonMap(to, expectedRate)));
        convertService.covert(from, to, amount);
        Mockito.verify(mathUtils, Mockito.times(1)).scale(eq(amount), argumentCaptor.capture());
        assertEquals(expectedRate, argumentCaptor.getValue(), 0.00001, "must be in delta");
    }
}