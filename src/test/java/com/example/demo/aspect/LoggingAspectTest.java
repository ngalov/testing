package com.example.demo.aspect;

import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.spi.ILoggingEvent;
import com.example.demo.client.ForexClient;
import com.github.tomakehurst.wiremock.client.WireMock;
import org.aspectj.lang.annotation.Before;
import org.junit.jupiter.api.Test;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static org.junit.jupiter.api.Assertions.assertTrue;


@SpringBootTest
@AutoConfigureWireMock(port = 4567)
class LoggingAspectTest {

    @Autowired
    private ForexClient forexClient;


    @Test
    public void logTrace() {
        Logger logger = (Logger) LoggerFactory.getLogger(LoggingAspect.class);
        StaticAppender staticAppender = new StaticAppender();
        logger.addAppender(staticAppender);
        staticAppender.start();

        stubFor(WireMock.get(urlEqualTo("/fetch-one?from=USD&to=EUR&api_key=3721e2ab07-a370c625a0-qr3qyt"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withBody("{\n" +
                                "  \"base\": \"USD\",\n" +
                                "  \"result\": {\n" +
                                "    \"EUR\": 0.84653\n" +
                                "  },\n" +
                                "  \"updated\": \"2021-04-05 17:43:35\",\n" +
                                "  \"ms\": 19\n" +
                                "}")));

        forexClient.getForexData("USD", "EUR");
        System.out.println("staticApp" + staticAppender.getEvents());
        assertTrue(staticAppender.getEvents().stream()
                .map(ILoggingEvent::getFormattedMessage)
                .anyMatch(msg -> msg.contains("ForexResult(base=USD, result={EUR=0.84653})")));
    }


}