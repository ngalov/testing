package com.example.demo.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Map;

@Data
@Accessors(chain = true)
public class ForexResult implements Serializable {
    private String base;
    private Map<String, Double> result;
}
