package com.example.demo.config;

import com.hazelcast.config.Config;
import com.hazelcast.config.MapConfig;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
public class CacheConfig {

    @Bean
    @Profile("!test")
    Config getConfig() {
        return new Config()
                .setInstanceName("demo")
                .addMapConfig(
                        new MapConfig()
                                .setName("forexResults")
                                .setTimeToLiveSeconds(5));
    }
}
