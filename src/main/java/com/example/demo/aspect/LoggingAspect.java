package com.example.demo.aspect;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

@Aspect
@Component
@Slf4j
public class LoggingAspect {

    @Around("@annotation(LogMethod)")
    public Object logMethod(ProceedingJoinPoint joinPoint) throws Throwable {
        log.info("method input : {}", joinPoint.getArgs());
        Object proceed = joinPoint.proceed();
        log.info("method result : {}", proceed);
        return proceed;
    }
}
