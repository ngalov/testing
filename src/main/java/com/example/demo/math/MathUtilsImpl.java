package com.example.demo.math;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
@Slf4j
public class MathUtilsImpl implements MathUtils {

    @Override
    public BigDecimal scale(BigDecimal amount, Double scale) {
        log.info("scale amount {} by {}", amount, scale);
        return amount.multiply(BigDecimal.valueOf(scale));
    }
}
