package com.example.demo.math;

import java.math.BigDecimal;

public interface MathUtils {

    BigDecimal scale(BigDecimal amount, Double rate);
}
