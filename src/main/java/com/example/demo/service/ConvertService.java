package com.example.demo.service;

import java.math.BigDecimal;

public interface ConvertService {

    /**
     * @param from base currency mnemonic
     * @param to target currency mnemonic
     * @param amount source amount
     * @return target amount
     */
    BigDecimal covert(String from, String to, BigDecimal amount);
}
