package com.example.demo.service;

import com.example.demo.client.ForexClient;
import com.example.demo.dto.ForexResult;
import com.example.demo.math.MathUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
@RequiredArgsConstructor
@Slf4j
public class ConvertServiceImpl implements ConvertService {

    private final MathUtils mathUtils;
    private final ForexClient forexClient;

    @Override
    public BigDecimal covert(String from, String to, BigDecimal amount) {
        return mathUtils.scale(amount, fetchForexResult(from, to).getResult().get(to));
    }

    public ForexResult fetchForexResult(String from, String to) {
        log.info("fetch Forex result from {} to {}", from, to);
        return forexClient.getForexData(from, to);
    }
}
