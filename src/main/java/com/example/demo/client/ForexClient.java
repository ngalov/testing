package com.example.demo.client;

import com.example.demo.aspect.LogMethod;
import com.example.demo.dto.ForexResult;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@Service
@RequiredArgsConstructor
public class ForexClient {

    private final RestTemplate restTemplate;

    public static final String FROM = "from";
    public static final String TO = "to";
    public static final String API_KEY = "api_key";

    @Value("${forex.api-key}")
    private String apiKey;
    @Value("${forex.url}")
    private String forexUrl;

    @LogMethod
    @Cacheable("forexResults")
    public ForexResult getForexData(String from, String to) {
        return restTemplate.exchange(
                    UriComponentsBuilder.fromHttpUrl(forexUrl)
                            .queryParam(FROM, from)
                            .queryParam(TO, to)
                            .queryParam(API_KEY, apiKey)
                            .toUriString(),
                    HttpMethod.GET,
                    HttpEntity.EMPTY,
                    ForexResult.class)
                .getBody();
    }
}
