package com.example.demo.controller;

import com.example.demo.dto.ConvertResult;
import com.example.demo.service.ConvertService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;

@RestController
@RequestMapping("converter")
@RequiredArgsConstructor
public class ConvertController {

    private final ConvertService convertService;

    @GetMapping
    public ResponseEntity<ConvertResult> convert(
            @RequestParam String from,
            @RequestParam String to,
            @RequestParam BigDecimal amount) {
        return ResponseEntity.ok(ConvertResult.builder()
                .sourceAmount(amount)
                .targetAmount(convertService.covert(from, to, amount))
                .build());
    }
}
